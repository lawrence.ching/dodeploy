# dodeploy

A simple deployment script for uber jar.

```bash
deploy <groupId>:<artifactId>:<version> /path/to/folder
```

## Installation

```bash
curl -s https://gitlab.com/lawrence.ching/dodeploy/raw/master/install.sh | bash
```
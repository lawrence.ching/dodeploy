#!/usr/bin/env bash

info () {
    echo "$(date -u +"%Y-%m-%dT%H:%M:%SZ") INFO - $1"
}

info 'Welcome to use dodeploy(1.0.0)'
info 'For any problem please visit https://gitlab.com/lawrence.ching/dodeploy'
info '---'
JAR=$1
DESTINATION=$2
parts=(${JAR//:/ })
GROUP_ID=${parts[@]:0:1}
ARTIFACT_ID=${parts[@]:1:1}
VERSION=${parts[@]:2:1}
JAR_NAME="$ARTIFACT_ID-$VERSION.jar"
CURRENT_DIR="$pwd"

info "Start to deploy $JAR to $DESTINATION"

info "Downloading $GROUP_ID:$ARTIFACT_ID:$VERSION"

mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.1:copy -Dartifact=$JAR -DoutputDirectory=$DESTINATION

cat << EOF > $DESTINATION/start.sh
#!/usr/bin/env bash

set -e
set -x

PIDFILE="pid"

if [ -f "\$PIDFILE" ]; then
    echo Cannot start as it may already be running. Run stop.sh first.
    exit 1
fi

mkdir -p ./logs
nohup java -jar $JAR_NAME > logs/console.log 2>&1 < /dev/null &
echo \$! > \$PIDFILE
echo $JAR_NAME started
EOF
info "Generated start.sh at $DESTINATION"

chmod +x $DESTINATION/start.sh
info "Granted execute permission to start.sh"

cat << EOF_STOP_SH > $DESTINATION/stop.sh
#!/usr/bin/env bash

set -x

PIDFILE="pid"

if [ -s "\$PIDFILE" ]; then
    kill -9 \$(cat \$PIDFILE)
    rm -f \$PIDFILE
    echo Stopped
else
    echo There is no pid file so there is nothing to stop.
    exit 1
fi
EOF_STOP_SH
info "Generated stop.sh at $DESTINATION"
chmod +x $DESTINATION/stop.sh
info "Granted execute permission to stop.sh"




if ! [ -x "$(command -v mvn)" ]; then
  echo 'Error: maven is not found.' >&2
  exit 1
fi

mkdir -p ~/.bin
FILE=~/.bin/dodeploy.sh
curl https://gitlab.com/lawrence.ching/dodeploy/raw/master/src/deploy-local.sh > $FILE
chmod +x $FILE

cat << EOF >> ~/.bashrc
alias deploy="$FILE"
alias dodeploy="$FILE"
EOF

source ~/.bashrc

